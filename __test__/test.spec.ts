import router from "../src/router/router";

require('babel-plugin-require-context-hook/register')();

describe("Filter function", () => {
    test("default test", () => {
        expect(router.currentRoute.path).toEqual("/");
    });
});