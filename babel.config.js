module.exports = {
  presets: [
      '@vue/app',
      "@babel/preset-typescript"
  ],
  plugins: [
    "require-context-hook"
  ]
};