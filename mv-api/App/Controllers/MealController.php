<?php
namespace App\Controllers;

class MealController extends Controller {
    public function index() {
        $db = new \Database\Connector("localhost", "root", "martin", "grocery-manager");
        
        $sqlString = "SELECT * FROM meal";
        $result = $db->query($sqlString);
        $resultArray = $result->fetch_all(MYSQLI_ASSOC);
        $this->respond(json_encode($resultArray));
    }
}