import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import store from "../store";
const VideoView = () => import("../views/VideoView.vue");
const TypeSelection = () => import("../views/TypeSelection.vue");
const Login = () => import("../components/authentication/Login.vue");

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next();
    return;
  }
  next("/");
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return;
  }
  next("/login");
};

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "selection",
    component: TypeSelection,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/video/:seriesId/:id?",
    name: "Video",
    component: VideoView,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    beforeEnter: ifNotAuthenticated,
  }
];

const router = new VueRouter({
  routes
});

export default router;
