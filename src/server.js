import { Server, Model } from "miragejs";

export function makeServer({ environment = "development" } = {}) {
  const server = new Server({
    environment,

    models: {
      user: Model,
      token: Model
    },

    seeds(server) {
      server.create("user", { name: "doggo", title: "sir" });
      server.create("token", { token: "testToken" });
    },

    routes() {
      this.get("/user/me", schema => {
        return schema.users.all();
      });

      this.post("/auth", () => {
        return { token: "testToken" };
      });
    }
  });

  return server;
}
