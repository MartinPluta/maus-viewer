export default class ImageService {
  public getRandomProtagonistImage(): string {
    const imageList: string[] = this.getProtagonistImages();
    return imageList[Math.floor(Math.random() * imageList.length)];
  }

  public getProtagonistImages(): string[] {
    const images = require.context(
      "@/assets/images/protagonists/transparent",
      true,
      /^.*\.gif$/
    );
    return images.keys().map(source => source.replace("./", ""));
  }
}
