import router from "../router/router";

export default class VideoRedirect {
  public getRandomVideoSource(subDirectory: string): string {
    const videoList: string[] = this.getVideoList(subDirectory);
    return videoList[Math.floor(Math.random() * videoList.length)];
  }

  public getVideoList(subDirectory: string): string[] {
    const videos = require.context("@/assets/videos", true, /^.*\.mp4$/);
    const videoList = videos
      .keys()
      .map(source => source.replace("./", ""))
      .filter(item => item.includes(subDirectory));
    return videoList;
  }

  public goToRandomVideo(seriesId: string) {
    const videoId = this.getRandomVideoSource(seriesId)
      .replace(".mp4", "")
      .replace("maus/", "");
    router.push({ path: "/video/maus/" + videoId });
  }
}
