import Vue from "vue";
import Vuex from "vuex";
import user from "./modules/user";
import auth from "./modules/auth";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    seriesTypes: ["maus", "plonsters"],
    seriesId: "maus",
    selectNextVideo: false
  },
  mutations: {
    updateselectNextVideo(state, newValue) {
      state.selectNextVideo = newValue;
    }
  },
  actions: {},
  modules: {
    user,
    auth
  },
});
