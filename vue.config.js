const path = require('path');

module.exports = {
  css: {
    loaderOptions: {
      scss: {
        prependData:
          '@import "~@/styles/_animations.scss";\n                      @import "~@/styles/_resets.scss";\n                      @import "~@/styles/_variables.scss";'
      }
    }
  },
  transpileDependencies: ["vuetify"],
  configureWebpack: {
    resolve: {
      alias: {
        components: path.resolve(__dirname, 'src/components/'),
        actions: path.resolve(__dirname, 'src/store/actions'),
        utils: path.resolve(__dirname, 'src/utils')
      }
    }
  }
};
